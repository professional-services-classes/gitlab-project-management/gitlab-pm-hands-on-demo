**GitLab Project Management Hands On Guide**

**List of Labs for this course: **

[Complete Hands On Guide ](https://drive.google.com/file/d/1N0R0edeHAwCjmcc7UoL241oTWbJg0dkY/view?usp=sharing)

- [ ] LAB 1- Review an Example GitLab Issue Section 
- [ ] LAB 2- Create a Sub-Group and Project
- [ ] LAB 3- Create an Issue and Add Details to it 
- [ ] LAB 4- Createa a Parent Epic and Sub-Epics 
- [ ] LAB 5- Create and Assign Milestones 
- [ ] LAB 6- Create a Detailed Wiki Page 
- [ ] LAB 7- Create New Epic and Issue Boards


