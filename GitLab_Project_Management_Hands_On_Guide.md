﻿![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.001.png) **GitLab Hands On Guide** 

![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.001.png) **Gitlab Hands On Guide ![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.002.png)**

![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.003.png) **TABLE OF CONTENTS**

[**GITLAB PM LAB SETUP](#_page2_x90.00_y138.75) **[6**](#_page2_x90.00_y138.75)** [**LAB 1- REVIEW EXAMPLE GITLAB ISSUE SECTION](#_page3_x90.00_y88.50) **[7**](#_page3_x90.00_y88.50)** [**LAB 2- CREATE A SUB-GROUP AND PROJECT](#_page3_x90.00_y488.25) **[7**](#_page3_x90.00_y488.25)** [**LAB 3- CREATE AN ISSUE AND ADD DETAILS TO IT](#_page5_x90.00_y338.25) **[9**](#_page5_x90.00_y338.25)** [**LAB 4- CREATE A PARENT EPIC AND SUB-EPICS](#_page6_x90.00_y88.50) **[10**](#_page6_x90.00_y88.50)** [**LAB 5- CREATE AND ASSIGN MILESTONES](#_page8_x90.00_y330.00) **[12**](#_page8_x90.00_y330.00)** [**LAB 6- CREATE A WIKI PAGE](#_page9_x90.00_y272.25) **[13](#_page9_x90.00_y272.25) [LAB 7- CREATE A NEW BOARD](#_page10_x90.00_y88.50) [13**](#_page10_x90.00_y88.50)** [**REVIEW QUIZ](#_page11_x90.00_y88.50) **[14**](#_page11_x90.00_y88.50)** [**QUESTION & ANSWER SESSION](#_page11_x90.00_y159.00) **[15**](#_page11_x90.00_y159.00)** 
Page PAGE2 of NUMPAGES13 <https://about.gitlab.com/services/> 
![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.001.png) **Gitlab Hands On Guide** 

![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.004.png) **GITLAB PROJECT MANAGEMENT LAB**

1. **GITLAB PM LAB SETUP**  
1. To begin lab, navigate to the sandbox link: <https://www.gitlabdemo.com/invite>  
1. Enter your invitation code in the box and Click Redeem. 

![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.005.jpeg)

3. You will be brought to the link below: 
3. Click Download Credentials. 
3. Click the GitLab URL and launch your demo environment. 

![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.006.jpeg)

6. Enter your credentials and launch the demo environment. 
Page PAGE3 of NUMPAGES13 <https://about.gitlab.com/services/> 
![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.001.png) **GitLab Hands On Guide** 

2. **LAB 1- REVIEW EXAMPLE GITLAB ISSUE SECTION**  
1. **Step 1: Navigate to the following URL- <https://gitlab.com/gitlab-org/gitlab/-/issues>**  

![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.007.jpeg)

2. **Step 2: Review the Issues List, Boards, Milestones and Labels.** 

2.2.1. Hover over any of the columns and rows in the following sections to view more details.  

3. **Step 3:  Click on the Boards > Switchboard dropdown > 12.3 Release** 
   1. Review all of the Boards that display and the available filtering options.  
   1. Review any **of the issues** and take a look at the labels used throughout the project.  
3. **LAB 2- CREATE A SUB-GROUP AND PROJECT** 
1. **Step 1: Navigate to Groups -> Click on the groups**  
1. **Step 2: Navigate to your test group ![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.008.jpeg)**

3.2.1. Navigate to Training Users  and expand the arrow to the left of Training Users. 

3. **Step 3:  Create a Sub-Group  ![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.009.jpeg)**
1. Navigate to the New Project button and click the white arrow to open the available list. Click the **New Subgroup** from the drop-down and then click on the green **New Subgroup** button. 
1. In the Group Name field, type ‘*{company-name} New Initiative 1*’  
1. In the Group Description field, you can type any optional description for the sub-group, and then click the **Create Group** button.

![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.010.jpeg)

4. **Step 4:  Create a Project**  

3.3.1. Navigate to the **New Project** button and click it. In the Project name dialog box, enter “{**Name} PM Hands On”**.  Optionally you may include a few notes in the Project Description Dialog Box. -> Select **Internal**.  Click the **Initialize repository with a README** checkbox and then click the green **Create Project** button. This is your project view.  Note: If you do not initialize your 

repository with a README, you will have a stripped down Git rep, that you will need to create content to bring it into existence. 

![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.011.jpeg)

4. **LAB 3- CREATE AN ISSUE AND ADD DETAILS TO IT** 

**4.1. Step 1: Create an Issue**  

1. From the left pane of the dashboard:  Navigate to and click on Issue.  Click the green **New Issue** button. In the title dialog box -> enter: “*Project kick off*”. Optionally, enter a comment in the Description dialog box.  In the Assignee dialog box -> assign the issue to yourself.  
1. In the Weight field, **enter 10** and assign a due date for a week from today. Leave all other fields at their default and click the **Submit Issue** button. 
1. Step 2: Create 3 Labels 
1. From the left pane of your dashboard:  Navigate to Issues -> Labels -> Click the green New label button.  In the Title dialog box -> enter label name as:  “*workflow::todo*” -> assign it a background color (your choice) Click the green **Create label** button. Remember, your labels are created at the project level, so they are specific to that project level.  They will not be available at the group level. 
1. Repeat these steps 2 more times, using “workflow::resolved”  and “workflow::backlog”  in the title dialog box. 
5. **LAB 4- CREATE A PARENT EPIC AND SUB-EPICS** 
1. **Step 1: Navigate to your sub-group** 
1. Navigate to Training Users  and expand the arrow to the left of Training Users and click on your sub-group. 
1. On the left-hand navigation pane, click on the Epics section, then click the green **New Epic** button. 
1. In the Title field, type “*Project 1x Update*” and click **Create Epic**.  
1. ![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.012.jpeg)
1. In the right hand pane, begin filling in the details for the epic. In the Due date 

field, select a month from now.  

6. In the Upper right hand corner, click the **New Epic** button. 
6. In the title field, type “*Project 1x Developer Work*” and in the description field, type “*this epic will house all the work by developer team 1 for Project 1x.*” 
6. In the Labels dropdown- create a label for “*working::in development*” with a green label and select it to add it to the epic.  
6. Leave the start and due dates empty and click the **Create Epic** button. 
2. **Step 2- Create a Sub-Epic and tie it to the first one** 
1. On the left hand panel, click on Epics, then select your first epic for **Project 1x Update** 
2. Once the epic is open, in the Epics and Issues section, on the left click the Add drop down menu and select **Add an Existing Epic** and paste the URL from your second epic to add it as a Sub-Epic.  

![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.013.jpeg)

3. Click the green **New Epic** button and in the Title field, type “*Project 1x Marketing Work*” and click **Create Epic**.  
3. In the Comment Field, use a quick action to add this epic to the Parent epic. Type ***/parent\_epic <[&1​](https://gitlab-core.us.gitlabdemo.cloud/groups/training-users/iuzyocr1/test-new-initiative-1/-/epics/1)>***  and then select Project 1x Update from the drop down that appears.  

![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.014.jpeg)

5. Click the green **Comment** button. 
5. Step 3- Tie an issue to an epic  
5. Navigate to your **Project 1x Update** epic, in the right hand corner, click on the **Add** dropdown and select **Add an Existing Issue**.  
8. In the field that populates, type “ *#* ” and you will see a drop down appear, select your issue from the list and click the green **Add** button.  
8. ![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.015.jpeg)
6. **LAB 5- CREATE AND ASSIGN MILESTONES**  
1. **Step 1: Create 3 Milestones**  
1. In your GitLab Sub-Group or Epic, hover over the Issues section on the left hand pane and select Milestones from the menu that appears.  
1. Click the green **New milestone** button in the upper right corner.  
1. In the Title field, type “*Sprint 1*”, in the start date, select today and in the due date, select a date 2 weeks out. In the description field, type “*First sprint of development on Project 1x, x, y, z project outcomes are due*”  
1. Click **Create milestone**. 
1. Repeat these steps for Sprints 2 and 3 and continue to phase out the start and due dates so they align two weeks apart.  
2. **Step 2: Add a Milestone to an Issue**  
1. On the left hand pane, click on Issues and then click on your open issue.  
1. Once you have your issue open, click on the **edit** icon next to Milestone and add the **Sprint 1** milestone to the issue.  
1. Navigate back to your sub-group and click on the **Project 1x Marketing Work** epic.  
1. In the upper right corner, click the Add dropdown and select **Add a New Issue** from the drop down.**   
5. In the Title field, type “*Marketing Material Drafts*” and in the drop down menu select your project and click **Create Issue**.  
5. Repeat the steps to create another issue titled, “*Marketing Material Approvals*”.  
5. Click on each issue and add a **weight of 30**, **workflow::todo label**, and **Sprint 1 Milestone** to both.  
5. Navigate back to the sub-group and hover over the **Issues** section and click on **Milestones** from the menu list.  
5. Review the available options for displaying the Burndown Chart and what data displays.  
7. **LAB 6- CREATE A WIKI PAGE** 
1. Navigate to your previously created project.  
1. Once you are in your project, in the left hand pane, locate the **Wiki** section and click it.  
1. Click the **Create your first page** button to open the editor.  
1. Fill in the details of your initiative and link your epics you have created inside the content and click the **Create Page** button. 

![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.016.jpeg)

8. **LAB 7- CREATE A NEW BOARD**  

**8.1. Step 1: Navigate to Groups -> Click on the groups** 

1. Navigate to your test group > Select your sub-group 
1. In the left hand pane hover over the Issues section and select Boards from the dropdown menu.  
1. In the fitler menu at the top of the board section, select the Development drop down menu and click on **Create new board**  

![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.017.jpeg)

4. In the Title, type, “*In Development*” and then click the **Expand** button next to Board scrope.  
4. In the Labels section, click the **Edit** hyperlink and select your **working::in development label**. 
4. Move columns around and organize the board to your liking.  
4. Click the **Edit Board** button to view the other options available.  

![](Aspose.Words.a16ec31e-5d06-42e9-a3bf-231e81c18252.018.jpeg)

9. **REVIEW QUIZ**  

Complete the [review quiz](https://forms.gle/HY35NgcEd7cWtSEh6) to test your knowledge of the course! 

10. **QUESTION & ANSWER SESSION**  

If you have any follow up questions regarding these labs, feel free to chat them directly to your instructor in the online class platform or you can email us at <Proserv-education@gitlab.com>. 
Page PAGE13 of NUMPAGES13 <https://about.gitlab.com/services/> 
